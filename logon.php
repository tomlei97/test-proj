<?php
$login_err = "";

// When submit button is clicked
if (isset($_POST['submit'])) {
  $uname = trim($_POST['uname']);
  $pwd = $_POST['pwd'];

  // If the username and password matches the records, a welcome message will appear.
  if ($pwd == 'vcn198?' && $uname == 'Tom'
      || $pwd == 'vcn198?' && $uname == 'Patrick') {
    session_start();
    $_SESSION['uname'] = $uname;
    header ("Location: ./welcome.php");
    exit;
  } else {
    $login_err = "Invalid Username of Password";
  }
}
?>

<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
form {border: 3px solid #17e62f;}

input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

.container {
  padding: 16px;
}

</style>
</head>
<body>  

<h2>Login</h2>
<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>"> 
  <div class="container">
    <span class="error"><?php echo $login_err;?></span>    
    <br><br>
    Username: <input type="text" name="uname">
    <br><br>
    Password: <input type="text" name="pwd">
    <br><br>
    <button type="submit" name="submit">Log On</button> 
  </div> 
</form>
</body>
</html>
